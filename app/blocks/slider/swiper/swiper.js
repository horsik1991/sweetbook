
(function () {
	let sliderMain = new Swiper('.slider',{
		speed: 600,
		loop: true,
		autoHeight: true,
		wrapperClass: 'slider__wrapper',
		slideClass: 'slider__slide',
		onResize: function () {
			sliderMain.update();
		},
		slideActiveClass: 'slider__slide--active',
		prevButton: '.slider__nav--prev',
		nextButton: '.slider__nav--next',
		pagination: '.slider__pagination',
		paginationClickable: true
	});
})();
